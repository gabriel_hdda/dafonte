﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaFonte.Models;
using DaFonte.Services;


namespace DaFonte.Controllers
{
    public class ContratoController : Controller
    {
        ContratoService _contratoService = new ContratoService();
        public ActionResult SearchFilial(string contratoNome)
        {
            List<Contrato> contrato = _contratoService.Search(contratoNome);

            return Json(contrato, JsonRequestBehavior.AllowGet);
        }
    }
}