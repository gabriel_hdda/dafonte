﻿using PowerOne.Models;
using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;

namespace PowerOne.Controllers
{
    [Authorize]
    public class ConfiguracoesController : Controller
    {
        #region Serviços
        private ConfiguracoesService _configuracoesService = new ConfiguracoesService();
        private AutorizacoesService _autorizacoesService = new AutorizacoesService();
        #endregion

        #region Temas
        public ActionResult Temas()
        {
            return View();
        }
        #endregion

        #region Trocar Icones
        [HttpPost]
        public ActionResult ChangeIcons(string icon)
        {
            if (icon != null)
            {
                string msg = _configuracoesService.ConfigurarTema(((Models.Login)Session["auth"]).Id.ToString(), null, icon);

                if (msg != null)
                {
                    TempData["Error"] = msg;
                    Log.Gravar(msg, Log.TipoLog.Erro);
                }
                else
                {
                    ((Models.Login)Session["auth"]).Tema = _configuracoesService.GetTema(((Models.Login)Session["auth"]).Id.ToString());
                    TempData["Success"] = "icones alterados com sucesso!";
                }
            }
            else
            {
                TempData["Error"] = "Selecione um icone!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }

            return RedirectToAction(nameof(Temas));
        }
        #endregion

        #region Configurar Cor do Menu
        [HttpPost]
        public ActionResult TrocarCorMenu(string color)
        {
            if (color != null)
            {
                string msg = _configuracoesService.ConfigurarTema(((Models.Login)Session["auth"]).Id.ToString(), color, null);

                if (msg != null)
                {
                    TempData["Error"] = msg;
                    Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
                }
                else
                {
                    ((Models.Login)Session["auth"]).Tema = _configuracoesService.GetTema(((Models.Login)Session["auth"]).Id.ToString());
                    TempData["Success"] = "Cor do menu alterado com sucesso!";
                }
            }
            else
            {
                TempData["Error"] = "Selecione uma cor!";
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }

            return RedirectToAction(nameof(Temas));
        }
        #endregion

        #region Autorizações
        public ActionResult Autorizacoes()
        {
            if (((Login)Session["auth"]).Autorizacoes.Tipo != "Adm")
            {
                return RedirectToAction("NaoAutorizado", "Home");
            }

            TempData["autorizacoes"] = _autorizacoesService.List();
            TempData["autorizacoesGrupo"] = _autorizacoesService.ListGrupo();

            return View();
        }

        [HttpPost]
        public ActionResult Autorizacoes(Autorizacoes autorizacoes)
        {
            string msg = _autorizacoesService.Update(autorizacoes);

            if (msg != null)
            {
                TempData["Error"] = msg;
                Log.Gravar((string)TempData["Error"], Log.TipoLog.Erro);
            }
            else
            {
                TempData["Success"] = $"Autorizações de {autorizacoes.UserName} atualizadas com sucesso.";
                ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
                ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
            }

            return RedirectToAction(nameof(Autorizacoes));
        }
        #endregion

        #region Autorizacoes para Grupo
        public ActionResult AtualizarAutorizacoesGrupo(AutorizacoesGrupo autorizacoes)
        {
            string msg = _autorizacoesService.AtualizarAutorizacoesGrupos(autorizacoes);

            if (msg != null)
            {
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Autorização atualizada com sucesso!";
                ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
                ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
            }

            return RedirectToAction("Autorizacoes");
        }
        #endregion

        #region Adicionar/Atualizar o grupo de um usuário
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AtualizarGrupoUsuario(string idUser, string idGrupo)
        {
            string msg = null;
            if (String.IsNullOrEmpty(idGrupo))
            {
                msg = "Selecione um grupo.";
            }
            else
            {
                msg = _autorizacoesService.AtualizarGrupoDoUsuario(idUser, idGrupo);
            }

            if (msg != null)
            {
                TempData["Error"] = msg;
            }
            else
            {
                TempData["Success"] = "Grupo de usuário adicionado com sucesso.";
                ((Login)Session["auth"]).Autorizacoes = _autorizacoesService.PegarAutorizacoes(((Login)Session["auth"]).Id.ToString());
                ((Login)Session["auth"]).Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(((Login)Session["auth"]).Autorizacoes);
            }


            return RedirectToAction(nameof(Autorizacoes));
        }

        #endregion
    }
}