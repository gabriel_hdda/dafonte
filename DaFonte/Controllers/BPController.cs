﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaFonte.Models;
using DaFonte.Services;

namespace DaFonte.Controllers
{
    public class BPController : Controller
    {
        #region services
        BPService _bPService = new BPService();
        #endregion

        FilialService _filiaisService = new FilialService();

        public ActionResult SearchBp(string BPName, string filial)
        {
            List<BP> BP = _bPService.Search(BPName, filial);

            return Json(BP, JsonRequestBehavior.AllowGet);
        }
    }
}