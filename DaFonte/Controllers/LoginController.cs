﻿using System;
using System.Data;
using System.Net.Mail;
using System.Web.Mvc;
using System.Web.Security;
using Antlr.Runtime.Misc;
using Microsoft.Ajax.Utilities;
using PowerOne.Models;
using PowerOne.Models.ViewModel;
using PowerOne.Services;
using PowerOne.Util;
using System.Timers;

namespace PowerOne.Controllers
{
    public class LoginController : Controller
    {
        public static System.Timers.Timer _timer;

        #region Serviços(Consultas ao Banco de dados)
        private LoginService _loginService = new LoginService();
        //private Email _email = new Email();
        private ConfiguracoesService _configuracoesService = new ConfiguracoesService();
        private AutorizacoesService _autorizacoesService = new AutorizacoesService();
        #endregion

        #region GET Login 
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region POST Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Login login)
        {

            if (!ModelState.IsValid)
            {
                return View(login);
            }

            string validado = isValid(login);
            if (validado == "Valid")
            {

                FormsAuthentication.SetAuthCookie(login.User, false);

                if (login.Password.ToUpper() == "NOVO")
                {
                    return RedirectToAction(nameof(AlterarSenha));
                }

                //_timer = new System.Timers.Timer();
                //_timer.AutoReset = false;
                //_timer.Interval = 900000;
                //_timer.Elapsed += (sender, e) => Desloga_Tempo(sender, e, login);
                //_timer.Enabled = true;

                return RedirectToAction("Index", "Home");
            }
            else if (validado == "Invalid")
            {
                ModelState.AddModelError("User", "Nome de Usuário e/ou Senha incorreto(s)!");
                return View();
            }
            else /*if (validado == "Authorized")*/
            {
                ModelState.AddModelError("User", "Seu usuário não tem nenhuma licença atribuída!");
                return View();
            }
            //else
            //{
            //    ModelState.AddModelError("User", "Licença Inválida, contate o fornecedor!");
            //    return View();
            //}

        }
        #endregion

        #region Validação
        private string isValid(Login login)
        {
            // valida licença
            //bool licenca = _licencaService.ValidaLicenca(null);

            //if (!licenca)
            //{
            //    return "Licence";
            //}

            string id_User = _loginService.ConsultaId(login); /*consulta o Id da OHEM*/
            if (id_User != null)
            {
                bool temCadastro = _loginService.ConsultaCadastro(id_User); /*consulta na P1_ACESS o id*/
                if (!temCadastro)
                {
                    string msg = _loginService.AddAcess(login, id_User); /*atualiza caso nao exista*/
                }
            }

            Login user = _loginService.Login(login);

            if (user != null)
            {

                Tema tema = _configuracoesService.GetTema(user.Id.ToString());
                if (tema != null)
                {
                    user.Tema = tema;
                }
                else
                {
                    _configuracoesService.ConfigurarTema(user.Id.ToString(), "BLACK", "PRO");
                    user.Tema = _configuracoesService.GetTema(user.Id.ToString());
                }

                _autorizacoesService.CriarAutorizacoes(user);

                user.Autorizacoes = _autorizacoesService.PegarAutorizacoes(user.Id.ToString());

                if (user.Autorizacoes.Licensa == "não")
                {
                    return "Authorized";
                }

                user.Autorizacoes.Grupo = _autorizacoesService.PegarGrupoAutorizacoes(user.Autorizacoes);

                //string mac = _loginService.GetMacAddress().ToString();
                //Session["MacAddress"] = mac;
                user.MACaddress = _loginService.GetMacAddress().ToString();
                string auxiliar = _loginService.AddValorLogin(user);

                Session["auth"] = user;

                return "Valid";
            }
            else
            {
                return "Invalid";
            }
        }
        #endregion

        #region Alteração de Senha
        public ActionResult AlterarSenha()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AlterarSenha(AlterarSenhaViewModel alterarSenha)
        {
            if (!ModelState.IsValid)
            {
                return View(alterarSenha);
            }
            Login paramsLogin = new Login() { User = alterarSenha.User, Password = alterarSenha.CurrentPassword };
            Models.Login user = _loginService.Login(paramsLogin);

            if (user != null)
            {
                _loginService.EditarSenha(alterarSenha);

                Session["auth"] = user;
            }
            else
            {
                ModelState.AddModelError("CurrentPassword", "Senha atual incorreta!");
                return View(alterarSenha);
            }

            TempData["Success"] = "Senha alterada com sucesso!";
            return RedirectToAction("Logout");
        }
        #endregion

        #region Recuperar Senha
        public ActionResult RecuperarSenha()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RecuperarSenha(RecuperarSenhaViewModel recuperarSenha)
        {
            if (!ModelState.IsValid)
            {
                return View(recuperarSenha);
            }

            //ConfiguracoesEmail configEmail = _configuracoesService.GetConfiguracoesEmail();
            string IdUser = _loginService.GetIdUsuarioPorNomeEmail(recuperarSenha.FirstName, recuperarSenha.LastName, recuperarSenha.Email);
            if (String.IsNullOrEmpty(IdUser))
            {
                ModelState.AddModelError("Email", "Os dados inseridos não correspondem a um usuário cadastrado, VerIfique seu cadastro no SAP!");
                return View(recuperarSenha);
            }
            else
            {
                //editar senha
                AlterarSenhaViewModel paramsPassword = new AlterarSenhaViewModel()
                {
                    Id = int.Parse(IdUser),
                    //gerando senha com 10 caracteres
                    NewPassword = GeradorDeSenha.GetSenha(10)
                };

                _loginService.EditarSenha(paramsPassword);
                //_email.RecoverPasswordEmail(paramsPassword.NewPassword, recuperarSenha.Email, configEmail);

                TempData["Success"] = "Você recebeu um email com sua nova senha!";
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Logout
        public ActionResult Logout()
        {
            string aux = _loginService.logoutLogLogin((Login)Session["auth"]);
            FormsAuthentication.SignOut();
            return Redirect(nameof(Index));
        }

        public ActionResult AutoLogout(Login login)
        {
            //string aux = _loginService.logoutLogLogin(login);
            //FormsAuthentication.SignOut();
            return Redirect(nameof(Logout));
        }

        #endregion

        #region VERIFICAR SE O USUÁRIO ESTÁ LOGADO
        [HttpPost]
        public ActionResult VerifyIfIsLogged()
        {

            bool isLogged = _loginService.ConsultaLogLogin((Login)Session["auth"]);

            if (!isLogged)
            {
                FormsAuthentication.SignOut();
                return Json(((Login)Session["auth"]).User + " logado em outro Dispositivo! Efetue o Login novamente.", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("logged", JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region contador de tempo

        //public void Desloga_Tempo(object sender, System.Timers.ElapsedEventArgs e, Login login)
        //{
        //    _timer.Enabled = false;
        //    Login LoGlOGADO = _loginService.Login(login);
        //    string aux = _loginService.logoutLogLogin(LoGlOGADO);
        //    _timer.Enabled = true;
        //    AutoLogout(login);

        //}
        #endregion
    }
}