﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DaFonte.Models;
using DaFonte.Services;

namespace DaFonte.Controllers
{
    public class FilialController : Controller
    {
        FilialService _filiaisService = new FilialService();

        public ActionResult SearchFilial(string BPLName)
        {
            List<Filial> filial = _filiaisService.Search(BPLName);

            return Json(filial, JsonRequestBehavior.AllowGet);
        }
    }
}