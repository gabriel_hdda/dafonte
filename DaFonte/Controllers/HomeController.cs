﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PowerOne.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        #region services
        #endregion

        #region overview
        public ActionResult Index()
        {
            if (((PowerOne.Models.Login)Session["auth"]) == null)
            {
                return RedirectToAction("Index", "Login");
            }

            return View();
        }
        #endregion

        #region Timesheet
        public ActionResult Timesheet()
        {
            return View();
        }
        #endregion

        #region Configurações
        public ActionResult Configuracoes()
        {
            return View();
        }

        #endregion

        #region Não Autorizado
        public ActionResult NaoAutorizado()
        {
            return View();
        }
        #endregion

    }
}