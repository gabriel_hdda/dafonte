﻿using DaFonte.Models;
using PowerOne.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace DaFonte.Services
{
    public class AtividadeService
    {
        public List<Atividade> List()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<Atividade> atividades = new List<Atividade>();

            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("SELECT \"Code\", \"Name\" FROM OCLS");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                Atividade atividade = new Atividade();
                atividade.AtCode = oRecordset.Fields.Item("Code").Value.ToString();
                atividade.AtName = oRecordset.Fields.Item("Name").Value.ToString();

                atividades.Add(atividade);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return atividades;
        }
    }
}