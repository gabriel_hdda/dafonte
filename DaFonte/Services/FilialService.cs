﻿using DaFonte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using PowerOne.Database;

namespace DaFonte.Services
{
    public class FilialService
    {
        #region Search
        public List<Filial> Search(string name)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<Filial> list = new List<Filial>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"BPLId\", \"BPLName\" FROM OBPL WHERE NOT \"BPLId\" = 2 AND \"Disabled\" = 'N' ");
            SQL.AppendLine("AND ((\"BPLId\" like '%" + name + "%') OR( \"BPLName\" like '%" + name + "%')) ");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                Filial item = new Filial();
                item.BplId = int.Parse(oRecordset.Fields.Item("BPLId").Value.ToString());
                item.BplName = oRecordset.Fields.Item("BPLName").Value.ToString();
                list.Add(item);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion

       
    }
}