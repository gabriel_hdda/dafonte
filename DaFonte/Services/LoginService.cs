﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Models.ViewModel;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Timers;

namespace PowerOne.Services
{
    public class LoginService
    {

        #region Consulta valor log login

        public bool ConsultaLogLogin(Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            bool aux = true;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"U_P1_Status\", \"U_P1_MACaddress\" FROM \"@P1_ACCESS\" WHERE \"U_P1_EMPId\" = " + login.Id + " ");

            oRecordSet.DoQuery(SQL.ToString());


            if (!oRecordSet.EoF)
            {
                oRecordSet.MoveFirst();
                if (oRecordSet.Fields.Item("U_P1_MACaddress").Value.ToString() != "")
                {
                    if (oRecordSet.Fields.Item("U_P1_MACaddress").Value.ToString() != login.MACaddress)
                    {
                        aux = false;
                    }
                    else
                    {
                        aux = true;
                    }
                }

                //if (oRecordSet.Fields.Item("U_P1_Status").Value.ToString() != "")
                //{
                //    if (oRecordSet.Fields.Item("U_P1_Status").Value.ToString() == "D")
                //    {
                //        aux = false;
                //    }
                //    else
                //    {
                //        aux = true;
                //    }
                //}
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return aux;
        }
        #endregion

        #region Logout update login
        public string logoutLogLogin(Login login)
        {
            string msg = null;

            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_ACCESS");

            if (oAccess.GetByKey(login.Id.ToString()))
            {
                oAccess.UserFields.Fields.Item("U_P1_Status").Value = "D";

                int iRetcode = oAccess.Update();

                if (iRetcode != 0)
                {
                    msg = oCompany.GetLastErrorDescription();
                    Log.Gravar(msg, Log.TipoLog.Erro);
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
            oAccess = null;

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region add valor logado
        public string AddValorLogin(Login login)
        {
            string msg = null;

            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_ACCESS");

            if (oAccess.GetByKey(login.Id.ToString()))
            {
                oAccess.UserFields.Fields.Item("U_P1_Status").Value = "L";
                oAccess.UserFields.Fields.Item("U_P1_MACaddress").Value = login.MACaddress.ToString();

                int iRetcode = oAccess.Update();

                if (iRetcode != 0)
                {
                    msg = oCompany.GetLastErrorDescription();
                    Log.Gravar(msg, Log.TipoLog.Erro);
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
            oAccess = null;

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;

        }
        #endregion

        #region Consulta de Usuario para Login
        public Login Login(Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            Login user;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT T0.\"empID\", concat(concat(T0.\"firstName\", '.'), T0.\"lastName\") AS \"Name\",");
            SQL.AppendLine("coalesce(T0.\"BPLId\",1) \"BPLId\", coalesce(T0.\"email\",'') \"email\" ,coalesce(T0.\"userId\",-1) \"userId\"");
            SQL.AppendLine("FROM OHEM T0 ");
            SQL.AppendLine("INNER JOIN \"@P1_ACCESS\" T1 ON T1.\"U_P1_EMPId\" = T0.\"empID\" ");
            SQL.AppendLine("WHERE upper(concat(concat(T0.\"firstName\", '.'), T0.\"lastName\")) = upper('" + login.User + "') ");
            SQL.AppendLine("AND (T1.\"U_P1_Password\" = '" + Criptografia.CalculateSHA1(login.Password) + "' OR ");
            SQL.AppendLine("(T1.\"U_P1_Password\" = 'NOVO' and T1.\"U_P1_Password\" = '" + login.Password + "'))");

            oRecordSet.DoQuery(SQL.ToString());

            if (!oRecordSet.EoF)
            {
                oRecordSet.MoveFirst();
                user = new Login()
                {
                    Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
                    User = oRecordSet.Fields.Item("Name").Value.ToString(),
                    IdFilial = int.Parse(oRecordSet.Fields.Item("BPLId").Value.ToString()),
                    UserId = int.Parse(oRecordSet.Fields.Item("UserId").Value.ToString())
                };
            }
            else
            {
                user = null;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return user;
        }
        #endregion

        #region Alterar Senha
        public void EditarSenha(AlterarSenhaViewModel alterarSenha)
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_ACCESS");

            if (oAccess.GetByKey(alterarSenha.Id.ToString()))
            {
                oAccess.UserFields.Fields.Item("U_P1_Password").Value = Criptografia.CalculateSHA1(alterarSenha.NewPassword);

                int iRetcode = oAccess.Update();

                if (iRetcode != 0)
                {
                    string sErrMessage = oCompany.GetLastErrorDescription();
                    Log.Gravar(sErrMessage, Log.TipoLog.Erro);
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
            oAccess = null;
        }
        #endregion

        #region Pegar Usuários por: Primeiro Nome, Últim Nome e Email
        public string GetIdUsuarioPorNomeEmail(string firstName, string lastName, string email)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string Id = null;
            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"empID\" FROM OHEM ");
            SQL.AppendLine("WHERE \"email\" = '" + email + "' ");
            SQL.AppendLine("AND \"firstName\" = '" + firstName + "' ");
            SQL.AppendLine("AND \"lastName\" = '" + lastName + "'");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                Id = oRecordset.Fields.Item("empID").Value.ToString();
                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return Id;
        }
        #endregion

        #region Pegar Id Todos os Usuários 
        public List<Login> GetAllIdUsers()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            List<Login> usersId = new List<Login>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"empID\", \"firstName\" FROM \"OHEM\"");

            oRecordSet.DoQuery(SQL.ToString());
            while (!oRecordSet.EoF)
            {
                usersId.Add(
                    new Models.Login()
                    {
                        Id = int.Parse(oRecordSet.Fields.Item("empID").Value.ToString()),
                        User = oRecordSet.Fields.Item("FirstName").Value.ToString()
                    });
                oRecordSet.MoveNext();
            }


            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
            oRecordSet = null;

            return usersId;
        }
        #endregion

        #region Atualizar Senha de Usuários já cadastrados
        public void ToFillTheEmptyPassword()
        {
            SAPbobsCOM.Company oCompany;
            oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oAccess = oCompany.UserTables.Item("P1_ACCESS");
            //string senhaCript = "NOVO";
            //senhaCript += Criptografia.Encrypt(senhaCript, true);
            List<Models.Login> users = GetUserEmptyPasswords();

            if (users != null)
            {
                foreach (var user in users)
                {
                    if (oAccess.GetByKey(user.Id.ToString()))
                    {
                        oAccess.UserFields.Fields.Item("U_P1_Password").Value = "NOVO";

                        int iRetcode = oAccess.Update();

                        if (iRetcode != 0)
                        {
                            string sErrMessage = oCompany.GetLastErrorDescription();
                        }
                    }

                    //System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
                    //oAccess = null;
                }
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oAccess);
            oAccess = null;

        }

        public List<Models.Login> GetUserEmptyPasswords()
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<Models.Login> userLogin = new List<Models.Login>();

            string SQL = "SELECT \"U_P1_EMPId\" FROM \"@P1_ACCESS\" WHERE \"U_P1_Password\" IS NULL";

            oRecordset.DoQuery(SQL);

            while (!oRecordset.EoF)
            {
                //oRecordset.MoveFirst();
                userLogin.Add(new Models.Login()
                {
                    Id = int.Parse(oRecordset.Fields.Item(0).Value.ToString())
                });

                oRecordset.MoveNext();
            }

            //if (!oRecordset.EoF)
            //{
            //    oRecordset.MoveFirst();
            //    userLogin.Add(new Models.Login()
            //    {
            //        Id = int.Parse(oRecordset.Fields.Item(0).Value.ToString()),
            //        User = oRecordset.Fields.Item(1).Value.ToString()
            //    });
            //}
            //else
            //{
            //    userLogin = null;
            //}

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return userLogin;
        }

        #endregion

        #region pegar endereço mac
        public string GetMacAddress()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);
            string chars = "ABCDEFGHIJKM";
            string pass = "";
            Random random = new Random();
            for (int f = 0; f < 4; f++)
            {
                pass += chars.Substring(random.Next(0, chars.Length - 1), 1);
            }
            string aux1 = DateTime.Now.ToString(CultureInfo.InvariantCulture).Trim();
            string tratado = aux1.Replace("/", "").Trim().Replace(":", "").Trim().Replace(" ", "");
            string code = String.Concat(pass, tratado.Remove(0, 6));
            code = code.Substring(0, 12);
            return code;
        }
        #endregion

        #region ConsultaId OHEM
        public string ConsultaId(Login login)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
      
            string[] array = login.User.Split('.');

            string Id = null;
            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"empID\" FROM OHEM ");
            SQL.AppendLine("WHERE \"firstName\" = '" + array[0] + "' ");
            SQL.AppendLine("AND \"lastName\" = '" + array[1] + "'");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            while (!oRecordset.EoF)
            {
                Id = oRecordset.Fields.Item("empID").Value.ToString();
                oRecordset.MoveNext();
            }
            

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return Id;

        }
        #endregion

        #region consulta o id na p1acess
        public bool ConsultaCadastro(string idUser)
        {
            bool Valid;

            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"U_P1_EMPId\" FROM \"@P1_ACCESS\" "); 
            SQL.AppendLine("WHERE \"U_P1_EMPId\" = '" + idUser + "' ");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            if (!oRecordset.EoF)
            {
                Valid = true;
            }
            else
            {
                Valid = false;
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return Valid;
        }
        #endregion

        #region Add valor caso nao exista 
        public string AddAcess(Login login, string idUser)
        {
            string[] array = login.User.Split('.');
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oLoginAdd = oCompany.UserTables.Item("P1_ACCESS");

            string msg = null;

            if (!oLoginAdd.GetByKey(idUser.ToString()))
            {
                oLoginAdd.Code = idUser;
                oLoginAdd.Name = array[0];
                oLoginAdd.UserFields.Fields.Item("U_P1_EMPId").Value = idUser.ToString();
                oLoginAdd.UserFields.Fields.Item("U_P1_Password").Value = "NOVO";
                if (oLoginAdd.Add() != 0)
                {
                    msg = oCompany.GetLastErrorDescription();
                }
            }

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
              
        }
        #endregion
    }
}