﻿using DaFonte.Models;
using PowerOne.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace DaFonte.Services
{
    public class ContratoService
    {
        #region Search
        public List<Contrato> Search(string name)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<Contrato> list = new List<Contrato>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT \"CstmrCode\", \"ContractID\", \"CntctCode\", \"CstmrName\" FROM OCTR");
            SQL.AppendLine("WHERE((\"ContractID\" like '%" + name + "%') OR( \"CstmrName\" like '%" + name + "%'");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                Contrato item = new Contrato();
                item.contratoId = oRecordset.Fields.Item("ContractID").Value.ToString();
                item.NomeContrato = oRecordset.Fields.Item("CstmrName").Value.ToString();
                list.Add(item);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion
    }
}