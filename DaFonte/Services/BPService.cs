﻿
using DaFonte.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using PowerOne.Database;

namespace DaFonte.Services
{
    public class BPService
    {
        #region Search
        public List<BP> Search(string name, string filial)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            List<BP> list = new List<BP>();

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT a.\"CardCode\", a.\"CardName\", b.\"BPLId\" FROM OCRD as a");
            SQL.AppendLine("FULL OUTER JOIN CRD8  as b");
            SQL.AppendLine("ON a.\"CardCode\" = b.\"CardCode\" ");
            SQL.AppendLine("WHERE a.\"CardType\" = 'S' and b.\"BPLId\" = '" + filial + "' AND ");
            SQL.AppendLine("((a.\"CardCode\" like '%" + name + "%') OR( a.\"CardName\" like '%" + name + "%'))");
            SQL.AppendLine("ORDER BY a.\"CardName\" ASC");

            oRecordset.DoQuery(SQL.ToString());

            while (!oRecordset.EoF)
            {
                BP item = new BP();
                item.BpId = int.Parse(oRecordset.Fields.Item("CardCode").Value.ToString());
                item.BpName = oRecordset.Fields.Item("CardName").Value.ToString();
                list.Add(item);

                oRecordset.MoveNext();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return list;
        }
        #endregion


    }
}