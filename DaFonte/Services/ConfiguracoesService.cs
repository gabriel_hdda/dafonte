﻿using PowerOne.Database;
using PowerOne.Models;
using PowerOne.Util;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PowerOne.Services
{
    public class ConfiguracoesService
    {
        #region Configurações de Tema
        public string ConfigurarTema(string code, string cor, string icone)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.UserTable oConfig = oCompany.UserTables.Item("P1_TEMA");

            string msg = null;
            int iRet = -1;

            //O código do usuário é utilizado como chave primária
            bool bExiste = oConfig.GetByKey(code); //verificando se já existe

            if (cor != null)
            {
                oConfig.UserFields.Fields.Item("U_P1_Color").Value = cor;
            }

            if (icone != null)
            {
                oConfig.UserFields.Fields.Item("U_P1_Icones").Value = icone;
            }

            if (!bExiste)//se já existir
            {
                oConfig.Code = code;
                oConfig.Name = code;
                iRet = oConfig.Add();
            }
            else
            {
                iRet = oConfig.Update();
            }

            if (iRet != 0)
            {
                msg = oCompany.GetLastErrorDescription();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oConfig);
            oConfig = null;

            if (msg != null)
            {
                Log.Gravar(msg, Log.TipoLog.Erro);
            }

            return msg;
        }
        #endregion

        #region Pegar Configuracoes de Tema
        public Tema GetTema(string code)
        {
            SAPbobsCOM.Company oCompany = Conexao.Company;
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            Tema configTema = null;

            StringBuilder SQL = new StringBuilder();
            SQL.AppendLine("SELECT * FROM \"@P1_TEMA\"");
            SQL.AppendLine("WHERE \"Code\" = '" + code + "'");

            oRecordset.DoQuery(SQL.ToString());

            oRecordset.MoveFirst();
            if (!oRecordset.EoF)
            {
                configTema = new Tema(); //ele só não será null se existir configurações
                configTema.UserId = oRecordset.Fields.Item("Code").Value.ToString();
                configTema.Icon = oRecordset.Fields.Item("U_P1_Icones").Value.ToString();
                configTema.Color = oRecordset.Fields.Item("U_P1_Color").Value.ToString();
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
            oRecordset = null;

            return configTema;

        }
        #endregion
    }
}