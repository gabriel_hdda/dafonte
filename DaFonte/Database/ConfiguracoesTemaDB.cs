﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PowerOne.Util.CreateUDF;

namespace PowerOne.Database
{
    public class ConfiguracoesTemaDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_TEMA".ToUpper()))
            {
                /*Configurações de Tema*/
                _createUDT.AddUDT("P1_TEMA", "P1 - Configurações de Tema", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddFields()
        {
            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_Color", "@P1_TEMA"))
            {
                List<ValoresValidos> oValValidos = new List<ValoresValidos>();
                oValValidos.Add(new ValoresValidos { sValor = "BLACK", sDescricao = "Preto" });
                oValValidos.Add(new ValoresValidos { sValor = "BLUE", sDescricao = "Azul" });
                oValValidos.Add(new ValoresValidos { sValor = "DARKGREEN", sDescricao = "Verde Escuro" });
                oValValidos.Add(new ValoresValidos { sValor = "GREEN", sDescricao = "Verde Claro" });
                oValValidos.Add(new ValoresValidos { sValor = "ORANGE", sDescricao = "Laranja" });
                oValValidos.Add(new ValoresValidos { sValor = "PURPLE", sDescricao = "Roxo" });
                oValValidos.Add(new ValoresValidos { sValor = "YELLOW", sDescricao = "Amarelo" });
                oValValidos.Add(new ValoresValidos { sValor = "RED", sDescricao = "Vermelho" });
                bCriou = _createUDF.AddUDF("P1_Color", "P1_TEMA", "Cor do menu", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, null, null, oValValidos);
            }

            if (!_consultaGeral.VerifyField("P1_Icones", "@P1_TEMA"))
            {
                List<ValoresValidos> oValValidos = new List<ValoresValidos>();
                oValValidos.Add(new ValoresValidos { sValor = "PRO", sDescricao = "Profissional" });
                oValValidos.Add(new ValoresValidos { sValor = "CARTOON", sDescricao = "Cartoon" });
                bCriou = _createUDF.AddUDF("P1_Icones", "P1_TEMA", "Icones do Portal", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 100, null, null, oValValidos);
            }
        }
    }
}