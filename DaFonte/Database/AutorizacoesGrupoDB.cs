﻿using PowerOne.Services;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PowerOne.Util.CreateUDF;

namespace PowerOne.Database
{
    public class AutorizacoesGrupoDB
    {
        private GeralQuery _consultaGeral = new GeralQuery();
        private CreateUDT _createUDT = new CreateUDT();
        private CreateUDF _createUDF = new CreateUDF();
        private AutorizacoesService _autorizacoesService = new AutorizacoesService();

        public void CreateTable()
        {
            if (!_consultaGeral.VerifyTable("P1_AccGrp".ToUpper()))
            {
                _createUDT.AddUDT("P1_AccGrp", "P1 - Autorizações p/ Grupo", SAPbobsCOM.BoUTBTableType.bott_NoObject);
            }
        }

        public void AddFields()
        {
            List<ValoresValidos> oValoresValidos = new List<ValoresValidos>();

            oValoresValidos.Add(new ValoresValidos { sValor = "N", sDescricao = "Sem Autorização" });
            oValoresValidos.Add(new ValoresValidos { sValor = "R", sDescricao = "Acesso Restrito" });
            oValoresValidos.Add(new ValoresValidos { sValor = "T", sDescricao = "Acesso Total" });

            bool bCriou = false;

            if (!_consultaGeral.VerifyField("P1_ConsSol", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_ConsSol", "@P1_AccGrp", "Consulta Solicitação", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewSol", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_NewSol", "@P1_AccGrp", "Solicitação de Compra", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_PedComp", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_PedComp", "@P1_AccGrp", "Pedido de Compra", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewPedC", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_NewPedC", "@P1_AccGrp", "Criar Pedido Compra", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_ReqS", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_ReqS", "@P1_AccGrp", "Consulta Req Saida", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewReqS", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_NewReqS", "@P1_AccGrp", "Nova Requisição", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewReem", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_NewReem", "@P1_AccGrp", "Novo Reembolso", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_Reemb", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_Reemb", "@P1_AccGrp", "Novo Reembolso", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewMailM", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_NewMailM", "@P1_AccGrp", "Novo Modelo Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_MailMod", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_MailMod", "@P1_AccGrp", "Exibir Modelo Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_AstEnvio", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_AstEnvio", "@P1_AccGrp", "Assistente Envio", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_Client", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_Client", "@P1_AccGrp", "Listar Clientes", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_NewGPCnt", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_NewGPCnt", "@P1_AccGrp", "Criar Grupo Clientes", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_GPCnt", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_GPCnt", "@P1_AccGrp", "Exibir Grupos Clientes", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_LogEmail", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_LogEmail", "@P1_AccGrp", "Registro Envios Email", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
            if (!_consultaGeral.VerifyField("P1_RTCobran", "@P1_AccGrp"))
            {
                bCriou = _createUDF.AddUDF("P1_RTCobran", "@P1_AccGrp", "Relatório Cobranças", SAPbobsCOM.BoFieldTypes.db_Alpha, SAPbobsCOM.BoFldSubTypes.st_None, 1, "N", null, oValoresValidos);
            }
        }

        public void Seeds()
        {
            _autorizacoesService.CriarGrupos("Financas", "Financas");
            _autorizacoesService.CriarGrupos("Vendas", "Vendas");
            _autorizacoesService.CriarGrupos("Compras", "Compras");
            _autorizacoesService.CriarGrupos("Estoque", "Estoque");
        }
    }
}