﻿function toggleTableForm(element) {
	//verificar se os formularios estão escondidos
	var _tr = jQuery(element).parent().parent();

	var formVisivel = _tr.find("td:eq( 1 )").find(".input-table-form").first().hasClass('visible-false');
	if (formVisivel) {
		_tr.find("td").each(function () {
			jQuery(this).find('.input-table-form').each(function () {
				jQuery(this).removeClass('visible-false');
			});
			jQuery(this).find('.td-table-form').each(function () {
				jQuery(this).addClass('visible-false');
			});
		});

	} else {
		_tr.find("td").each(function () {
			jQuery(this).find('.input-table-form').each(function () {
				jQuery(this).addClass('visible-false');
			});
			jQuery(this).find('.td-table-form').each(function () {
				jQuery(this).removeClass('visible-false');
			});
		});
    }
}

function adicionarLinha() {
	var form = $('.formLinha');
	if (form.hasClass('form-hide')) {
		form.removeClass('form-hide');
	} else {
		form.addClass('form-hide');
    }
}


function escreverMensagemEmInput(id_span, msg) {
	$(id_span).text(msg);
	setTimeout(function () {
		$(id_span).text('');
	}, 4000);
}



window.onbeforeunload = confirmExit;
function confirmExit() {
	if (linhas.length > 0) {
		return "Deseja realmente sair desta página?";
	}
}

function checaDisable() {
	if (linhas.length > 0) {
		$('#botao').prop('disabled', false);
		$("#filial").prop('disabled', true);
		$("#pesquisar-fornecedores").prop('disabled', true);
		$("#botao-fornecedores").prop('disabled', true);
		$("#pesquisar-filial").prop('disabled', true);
		$("#botao-filial").prop('disabled', true);
		//return true;
	} 
}
function excluirLinha(_index) {
	linhas.splice(_index, 1);
	$('tbody tr').remove();
	if (linhas.length <= 0) {
		$('#botao').prop('disabled', true);
		$("#filial").prop('disabled', false);
		$("#bp").prop('disabled', false);
		$("#pesquisar-fornecedores").prop('disabled', false);
		$("#botao-fornecedores").prop('disabled', false);
		$("#pesquisar-filial").prop('disabled', false);
		$("#botao-filial").prop('disabled', false);
	}
	listar();
}

function isAnyEmpty(el) {
	var isEmpty = false, v;
	el.each(function (e) {
		v = $(this).val();
		if (v == "" || v == null || v == undefined) {
			isEmpty = true
		}
	});
	return isEmpty;
}

function escreverSucesso(msg) {
	$('#alert').text(msg);
	$('#alert').removeClass('alert-hide');
	$('#alert').addClass('alert-success');
	setTimeout(function () {
		$('#alert').removeClass('alert-success');
		$('#alert').text(msg);
		$(msg).text('');
	}, 1000);

}

function escreverMensagemEmHead(msg) {
	$('#alert').text(msg);
	$('#alert').removeClass('alert-hide');
	$('#alert').addClass('alert-danger');
	setTimeout(function () {
		$('#alert').removeClass('alert-danger');
		$('#alert').text(msg);
		$(msg).text('');
	}, 4000);
	window.scroll(0, 0);
}

function dataFormatada(dataInformada) {
	return (new Date(dataInformada + 'T00:00')).toLocaleDateString('pt-BR') ;
}

function mascara(o, f) {
	v_obj = o
	v_fun = f
	setTimeout("execmascara() ", 1)
}

function execmascara() {
	v_obj.value = v_fun(v_obj.value)
}

function moeda(v) {
	v = v.replace(/\D/g, "") // permite digitar apenas numero
	v = v.replace(/(\d{1})(\d{17})$/, "$1.$2") // coloca ponto antes dos ultimos digitos
	v = v.replace(/(\d{1})(\d{13})$/, "$1.$2") // coloca ponto antes dos ultimos 13 digitos
	v = v.replace(/(\d{1})(\d{10})$/, "$1.$2") // coloca ponto antes dos ultimos 10 digitos
	v = v.replace(/(\d{1})(\d{7})$/, "$1.$2") // coloca ponto antes dos ultimos 7 digitos
	v = v.replace(/(\d{1})(\d{1,2})$/, "$1,$2") // coloca virgula antes dos ultimos 2 digitos
	return v;
}

function confAdd(a) {
	if (a != true) {
		if (linhas.length >= 1) {
			window.onbeforeunload = confirmExit;
			function confirmExit() {
				if (document.getElementById("pesquisar-filial").value != "") {
					return "Deseja realmente sair desta página?";
				}
			}
		}
	}
}



function soNumeros(v) {
	return v.replace(/\D/g, "")
}

function isNumber(n) {
	return $.isNumeric(n);
}

function SomenteNumero(e, numeros) {
	
	var v = numeros.value;
	var m = v.match(/,/g);
	if (m && m.length >= 1) {
		if (e.keyCode == "44") {
			return false
		}
	}

	//versao mobile
	//if (e.keyCode == "162" || e.keyCode == "163" || e.keyCode == "17" || e.keyCode == "42" || e.keyCode == "18" ) {
	//	return false
	//}

	//computador
	var tecla = (window.event) ? event.keyCode : e.which;
	

	if ((tecla > 47 && tecla < 58)) return true;
	
	else {
		if (tecla == 8 || tecla == 44 || tecla == 13) return true;
		else return false;
	}
}

function fnc(_tr) {
	var id = jQuery(_tr).attr("data-id");
	var tipo = jQuery(_tr).attr("data-tipo");

	radiobtn = document.getElementById(id);
	radiobtn.checked = true;
	if (tipo == "item") {
		//console.log("errado");

		escreverItemInput(radiobtn);
	} else if (tipo == "Filial") {
		//console.log("certo");

		preencher(radiobtn, tipo);
		listarDepositos();
	} else if(tipo == "bp"){
		escreverBpInput(radiobtn);
	}
	

}

function addSucces(msg, a, b) {
	var BodyTxt = "";
	if (a) {
		BodyTxt = '<p>Deseja conferir o último documento adicionado? <br>Essa ação te levará até a página de detalhes.</p>' +
			'<a class="btn btn-group btn-success " href="./RequerimentoEstoque/Index">Ir para Listagem</a>'
	} else {
		BodyTxt = '<p>Deseja conferir o último documento adicionado? <br>Essa ação te levará até a página de detalhes.</p>' +
			'<a class="btn btn-group btn-success " href="./Index">Ir para Listagem</a>'
	}
	let timerInterval
	Swal.fire({
		icon: 'success',
		title: msg,
		html: BodyTxt,
		timer: 4300,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading()
			timerInterval = setInterval(() => {
				const content = Swal.getContent()
				if (content) {
					const b = content.querySelector('b')
					if (b) {
						b.textContent = Swal.getTimerLeft()
					}
				}
			}, 100)
		},
		onClose: () => {
			clearInterval(timerInterval)
		}
	}).then((result) => {
		if (result.dismiss === Swal.DismissReason.timer) {
		}

	})
}

/*function trocaClasse(elemento, antiga, nova) {
	elemento.classList.remove(antiga);
	elemento.classList.add(nova);
}

function showTitle() {
	var div = document.querySelector('div');
	trocaClasse(div, 'hideElement', 'showElement');
}*/

