﻿function listarItens() {
    var itemName = $('#pesquisar-item').val();

    $.ajax({
        url: '/Item/Search',
        type: 'get',
        dataType: 'json',
        data: {
            'itemName': itemName,
        },
        success: function (data) {
            listarItensRadio(data);
        },
    });
}

function listarItensRadio(listItens) {
    $('tbody#lista-tabela-itens tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td>" + "<input type='radio' name='item-radio' id='" + item.ItemCode + "' onchange='escreverItemInput(this)' value='" + item.ItemCode + "' data-itemname='" + item.ItemName + "' data-unidademedida='" + item.UnidadeMedida + "'>" + "</td>";
        row += "<td>" + item.ItemCode + "</td>";
        row += "<td>" + item.ItemName + "</td>";

        $('tbody#lista-tabela-itens').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='item' data-id='" + item.ItemCode + "'>" + row + "<tr>");
    });
}

function escreverItemInput(_radio) {
    $('#desc-item').val(jQuery(_radio).val());
    $('#pesquisar-item').val(jQuery(_radio).data("itemname"));
    $('#BuyUnitMsr').val(jQuery(_radio).data("unidademedida"));
    $('#itens-modal').modal('toggle');
}
//inicio input bp
function listarBp() {

    var CardName = $('#pesquisar-fornecedores').val();
    var Filial = $('#idFilial').val();
    console.log(Filial)
    console.log(CardName)

    $.ajax({
        url: '/BP/SearchBp',
        type: 'get',
        dataType: 'json',
        data: {
            'CardName': CardName,
            'Filial': Filial,
        },
        success: function (data) {
            listarBpRadio(data);
        },
    });
}

function listarBpRadio(listItens) {
    $('tbody#lista-tabela-bp tr').remove();
    listItens.forEach(function (item) {
        var row = ""
        row += "<td>" + "<input type='radio' name='item-radio' id='" + item.CardCode + "' onchange='escreverBpInput(this)' ' value='" + item.CardName + "' data-cardcode='" + item.CardCode + "'></td>";
        row += "<td>" + item.CardCode + "</td>";
        row += "<td>" + item.CardName + "</td>";

        $('tbody#lista-tabela-bp').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='bp' data-id='" + item.CardCode + "'>" + row + "<tr>");
    });
}


function escreverBpInput(_radio) {
    $('#pesquisar-fornecedores').val(jQuery(_radio).val());
    $('#idFonecdor').val(jQuery(_radio).data("cardcode"));
    $('#bp-modal').modal('toggle');
}
$(document).on('keydown', '#pesquisar-fornecedores', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        listarBp();
        $('#bp-modal').modal('toggle');
    }
});
//fim input bp

//inicio input filial
function SearchFilial() {
    var BPLName = $('#pesquisar-filial').val();
    $.ajax({
        url: '/Filial/SearchFilial',
        type: 'get',
        dataType: 'json',
        data: {
            'BPLName': BPLName,
        },
        success: function (data) {
            listarFilial(data);
        },
    });
}

function listarFilial(listItens) {
    $('tbody#lista-tabela tr').remove();
    listItens.forEach(function (filial) {
        var row = ""
        row += "<td>" + "<input type='radio' name='item-radio' id='" + filial.BplId + "' onchange='preencherFilial(this);listarDepositos()' value='" + filial.BplName + "' data-bplid='" + filial.BplId + "'></td>";
        row += "<td>" + filial.BplName + "</td>";
        row += "<td>" + filial.BplId + "</td>";

        $('tbody#lista-tabela').append("<tr class='tr tr-click' onclick='fnc(this)' data-tipo='filial' data-id='" + filial.BplId + "'>" + row + "<tr>");
    });
}

function preencherFilial(_radio) {
    $('#pesquisar-filial').val(jQuery(_radio).val());
    $('#idFilial').val(jQuery(_radio).data("bplid"));
    $('#modalItens').modal('toggle');
}

$(document).on('keydown', '#pesquisar-filial', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        SearchFilial();
        $('#modalItens').modal('toggle');
    }
});
$(document).on('keydown', '#pesquisar-item', function (tab) {
    if (tab.which == 9 || tab.which == 13) {
        listarItens();
        $('#itens-modal').modal('toggle');
    }
});