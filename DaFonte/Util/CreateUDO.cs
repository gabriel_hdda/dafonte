﻿using PowerOne.Database;
using PowerOne.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PowerOne.Util
{
    public class CreateUDO
    {
        #region Serviços de Consultas ao Banco de Dados
        GeralQuery _geralQuery = new GeralQuery();
        #endregion

        //public bool Create(string codeUDO, string sNomeTabela, string sNomeTabelaFilha, string sDescricaoTabela, SAPbobsCOM.BoUDOObjType oTipoTabela)
        //{
        //    bool bCriou = true;
        //    if (!_geralQuery.VerifyUDO(codeUDO))
        //    {
        //        SAPbobsCOM.Company oCompany = Conexao.Company;
        //        SAPbobsCOM.UserTablesMD oTabelaoUsuario;

        //        oTabelaoUsuario = (SAPbobsCOM.UserTablesMD)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables);

        //        System.Runtime.InteropServices.Marshal.ReleaseComObject(oTabelaoUsuario);
        //        oTabelaoUsuario = null;
        //        GC.Collect();
        //        bCriou = AddTbMasterData(sNomeTabela, sNomeTabelaFilha, sDescricaoTabela, SAPbobsCOM.BoUDOObjType.boud_Document);
        //    }

        //    return bCriou;
        //}

        #region Create UDO addMasterData
        public bool AddTbMasterData(string sNomeTabela, string sNomeTabelaFilha, string sDescricaoTabela, SAPbobsCOM.BoUDOObjType oTipoTabela)
        {
            bool bCriou = false;
            int iRetCode = -1;
            SAPbobsCOM.Company oCompany = Conexao.Company;


            // 'In case the table does not yet exist add it
            SAPbobsCOM.UserObjectsMD oTabelaoUsuario;
            oTabelaoUsuario = (SAPbobsCOM.UserObjectsMD)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD);


            try
            {
                oTabelaoUsuario.Code = sNomeTabela;
                oTabelaoUsuario.Name = sNomeTabela;
                oTabelaoUsuario.ObjectType = oTipoTabela;
                oTabelaoUsuario.TableName = sNomeTabela;
                oTabelaoUsuario.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES;
                oTabelaoUsuario.CanLog = SAPbobsCOM.BoYesNoEnum.tYES;
                oTabelaoUsuario.LogTableName = sNomeTabela.Substring(3, 3) + "A" + sNomeTabela.Substring(3, 3);
                oTabelaoUsuario.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES;
                oTabelaoUsuario.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tNO;
                oTabelaoUsuario.MenuCaption = sNomeTabela;
                oTabelaoUsuario.FatherMenuID = 3328;
                oTabelaoUsuario.Position = 10;
                oTabelaoUsuario.ChildTables.TableName = sNomeTabelaFilha;
                oTabelaoUsuario.FormColumns.FormColumnAlias = "DocEntry";
                oTabelaoUsuario.FormColumns.FormColumnDescription = "DocEntry";
                oTabelaoUsuario.FormColumns.Add();

                iRetCode = oTabelaoUsuario.Add();

                //Verifica se o campo ja existe e retorna True
                if (iRetCode == -2035)
                {
                    bCriou = true;
                }
                else if (iRetCode == 0)
                {
                    //  Mensagem.StatusBar("Objeto " + sNomeTabela + " criado com sucesso!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                    bCriou = true;
                }
                //Verifica erros                
                else if (iRetCode != 0)
                {
                    //    Mensagem.StatusBar("Falha ao criar Objeto " + sNomeTabela, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                    bCriou = false;
                    string erro = oCompany.GetLastErrorDescription();
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(oTabelaoUsuario);
                oTabelaoUsuario = null;
            }
            //Verifica Erros
            catch (Exception ex)
            {
                //  Mensagem.StatusBar("Falha ao criar Objeto " + sNomeTabela + " " + ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                bCriou = false;
            }


            return bCriou;
        }
        #endregion
    }
}