﻿using PowerOne.Database;

namespace PowerOne.Util
{
    public class CreateTablesAndFields
    {
        #region Main
        public static void Main()
        {
            /*Configuracoes de Tema do Site*/
            ConfigTema();

            /*Autorizações Grupo*/
            AutorizacoesGrupo();

            /*Autorizações*/
            Autorizacoes();

            /*Log de Login*/
            LogLogin();

            /*Seeds*/
            Seeds();
        }
        #endregion

        #region Log de Login
        public static void LogLogin()
        {
            LogLoginDB logLoginDB = new LogLoginDB();
            //logLoginDB.CreateTable();
            logLoginDB.AddFields();
        }
        #endregion

        #region Autorizacoes de Grupo
        public static void AutorizacoesGrupo()
        {
            AutorizacoesGrupoDB autorizacoesGrupoDB = new AutorizacoesGrupoDB();
            autorizacoesGrupoDB.CreateTable();
            autorizacoesGrupoDB.AddFields();
            autorizacoesGrupoDB.Seeds();
        }
        #endregion

        #region Autorizações
        public static void Autorizacoes()
        {
            AutorizacoesDB autorizacoesDB = new AutorizacoesDB();
            autorizacoesDB.CreateTable();
            autorizacoesDB.AddFields();
            autorizacoesDB.Seeds();
        }
        #endregion

        #region Configuracoes de Temas
        public static void ConfigTema()
        {
            ConfiguracoesTemaDB configuracoesTemaDB = new ConfiguracoesTemaDB();
            configuracoesTemaDB.CreateTable();
            configuracoesTemaDB.AddFields();
        }
        #endregion

        #region Seeds
        public static void Seeds()
        {
            Seeds seed = new Seeds();

            seed.EmployeeSeeds();
        }
        #endregion
    }
}