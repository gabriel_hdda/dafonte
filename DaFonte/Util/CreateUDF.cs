﻿using PowerOne.Database;
using System;
using System.Collections.Generic;

namespace PowerOne.Util
{
    public class CreateUDF
    {
        #region AddUDF
        public bool AddUDF(string sFieldName, string sTable, string sDescField, SAPbobsCOM.BoFieldTypes oFieldType, SAPbobsCOM.BoFldSubTypes oFieldSubType, int iSize, string sDefault, string linkedTable, List<ValoresValidos> ValoresVaidos)
        {
            SAPbobsCOM.Company oConnection = Conexao.Company;
            SAPbobsCOM.UserFieldsMD oUserField = (SAPbobsCOM.UserFieldsMD)oConnection.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);

            bool bCriou = false;
            int iRetCode = -1;
            string sErrMessage = null;
            try
            {
                oUserField.TableName = sTable;
                oUserField.Name = sFieldName;
                oUserField.Description = sDescField;
                oUserField.Type = oFieldType;
                oUserField.SubType = oFieldSubType;
                if (iSize != 0)
                {
                    oUserField.EditSize = iSize;
                }


                if (!string.IsNullOrEmpty(sDefault))
                {
                    oUserField.DefaultValue = sDefault;
                }

                if (!string.IsNullOrEmpty(linkedTable))
                {
                    oUserField.LinkedTable = linkedTable;
                }

                //Atribui os valores válidos
                if (ValoresVaidos != null)
                {
                    foreach (ValoresValidos oVV in ValoresVaidos)
                    {
                        oUserField.ValidValues.Value = oVV.sValor;
                        oUserField.ValidValues.Description = oVV.sDescricao;
                        oUserField.ValidValues.Add();
                    }
                }


                iRetCode = oUserField.Add();

                //Verifica se o campo já existe
                if (iRetCode != 0)
                {
                    sErrMessage = oConnection.GetLastErrorDescription();
                    Log.Gravar("Table: " + sFieldName + "-"+ sErrMessage, Log.TipoLog.Erro);
                    bCriou = false;
                }
                else
                {
                    bCriou = true;
                }
            }
            catch(Exception e)
            {
                bCriou = false;
                Log.Gravar("Table: " + sFieldName + "-" + e.Message, Log.TipoLog.Erro);
            }

            System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserField);
            oUserField = null;

            return bCriou;
        }
        #endregion

        public struct ValoresValidos
        {
            public string sValor;
            public string sDescricao;
        }
    }
}