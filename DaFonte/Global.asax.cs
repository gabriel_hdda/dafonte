using PowerOne.Database;
using PowerOne.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DaFonte
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Conexao.Connect();

            //LicencaService.CriptografaArquivo();

            /**************************************
             * Alterações no Banco de Dados: UDF & UDT
             **************************************/
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-br", true);
            CreateTablesAndFields.Main();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
        }
    }
}
