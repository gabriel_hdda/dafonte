﻿using System;

namespace PowerOne.Models
{
    public class Tema
    {
        public string UserId { get; set; }

        public string Color { get; set; }

        public string Icon { get; set; }
    }
}