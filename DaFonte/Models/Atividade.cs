﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaFonte.Models
{
    public class Atividade
    {
        public string AtCode { get; set; }
        public string AtName { get; set; }
    }
}