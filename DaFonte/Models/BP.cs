﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaFonte.Models
{
    public class BP
    {
        public int BpId { get; set; }
        public string BpName { get; set; }
    }
}